package com.barauna.builders.client.repository;

import com.barauna.builders.client.model.ClientModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientRepository extends JpaRepository<ClientModel, Long> {

    Page<ClientModel> findByCpfAndName(@Param("cpf") String cpf, @Param("name") String name, Pageable pageable);

    void deleteById(@Param("id") Long id);

}
