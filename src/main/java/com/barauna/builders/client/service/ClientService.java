package com.barauna.builders.client.service;

import com.barauna.builders.client.dto.ClientDTO;
import com.barauna.builders.client.model.ClientModel;
import com.barauna.builders.client.repository.ClientRepository;
import com.sun.istack.internal.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Calendar;

@Service
public class ClientService {

    @Autowired
    ClientRepository clientRepository;

    public Page<ClientModel> findByCpfAndName(@NotNull String cpf, @NotNull String name, int page, int size) {
        PageRequest pageRequest = PageRequest.of(page, size, Sort.Direction.ASC, "name");
        Page<ClientModel> clientModelPage = clientRepository.findByCpfAndName(cpf, name, pageRequest);

        clientModelPage.get().forEach(clientModel -> {
            clientModel.setAge(setAge(clientModel.getDateBirth()));
        });
        return clientRepository.findByCpfAndName(cpf, name, pageRequest);
    }

    public ClientDTO creat(ClientDTO dto) {

        ClientModel model = ClientModel.builder().cpf(dto.getCpf())
                .dateBirth(dto.getDateBirth())
                .name(dto.getName()).build();
        model = clientRepository.save(model);
        return ClientDTO.builder()
                .id(model.getId())
                .name(model.getName())
                .cpf(model.getCpf())
                .dateBirth(model.getDateBirth())
                .build();
    }

    public void delete(Long id) {
        clientRepository.deleteById(id);
    }

    public ResponseEntity update(ClientDTO client) {
        try {
            ClientModel model = ClientModel.builder()
                    .cpf(client.getCpf())
                    .name(client.getName())
                    .id(client.getId())
                    .dateBirth(client.getDateBirth())
                    .build();
            clientRepository.save(model);
            return ResponseEntity.ok().body(model);
        } catch (Exception exception) {
            return ResponseEntity.status(HttpStatus.BAD_GATEWAY).body(exception);
        }
    }

    public ResponseEntity patch(Long id, ClientDTO dto) {
        try {
            ClientModel model = clientRepository.findById(id).orElseThrow(Exception::new);
            ClientModel modelPatch = buildClient(dto, model);
            clientRepository.save(modelPatch);
            return
                    ResponseEntity.ok().body(
                            ClientDTO.builder().
                                    cpf(modelPatch.getCpf())
                                    .dateBirth(modelPatch.getDateBirth())
                                    .id(modelPatch.getId())
                                    .name(modelPatch.getName()).build()
                    );
        } catch (Exception exception) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND)
                    .body(exception.getMessage());
        }
    }


    private ClientModel buildClient(ClientDTO dto, ClientModel model) {

        if (dto.getDateBirth() != null) {
            model.setDateBirth(dto.getDateBirth());
        }
        if (dto.getName() != null) {
            model.setName(dto.getName());
        }

        return model;
    }


    private int setAge(LocalDate date) {
        Calendar today = Calendar.getInstance();
        int age = today.get(Calendar.YEAR) - date.getYear();

        int currentMonth = today.get(Calendar.MONTH) + 1;
        if ((currentMonth == date.getMonthValue()) && today.get(Calendar.DAY_OF_MONTH) < date.getDayOfMonth()
                || currentMonth < date.getMonthValue()) {
            age--;
        }
        return age;
    }


}
