package com.barauna.builders.client.dto;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;

@Builder
@Data
public class ClientDTO {
    private Long id;
    private String name;
    private String cpf;
    private LocalDate dateBirth;

}
