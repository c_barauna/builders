package com.barauna.builders.client.controller;

import com.barauna.builders.client.dto.ClientDTO;
import com.barauna.builders.client.model.ClientModel;
import com.barauna.builders.client.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.logging.Logger;

@RestController
@RequestMapping("crud-client/v1/client")
public class ClientController {
    private static final Logger logger = Logger.getLogger(String.valueOf(ClientController.class));

    @Autowired
    private ClientService clientService;

    @GetMapping
    public ResponseEntity<Page<ClientModel>> findClientes(@RequestParam String name,
                                                          @RequestParam String cpf, @RequestParam int page, @RequestParam int size) {
        Page<ClientModel> dto = clientService.findByCpfAndName(cpf, name, page, size);

        return ResponseEntity.ok(dto);
    }

    @PostMapping
    public ResponseEntity<ClientDTO> create(@RequestBody ClientDTO clientDTO) {
        ClientDTO dto = clientService.creat(clientDTO);
        return ResponseEntity.status(HttpStatus.CREATED).body(dto);
    }

    @DeleteMapping
    public ResponseEntity delete(@RequestParam Long id) {
        try {
            clientService.delete(id);
            return ResponseEntity.ok().build();
        } catch (Exception exception) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(exception.getMessage());
        }
    }

    @PutMapping
    public ResponseEntity update(@RequestParam Long id, @RequestBody ClientDTO client) {
        try {
            ResponseEntity<ClientDTO> dto = clientService.update(client);
            return dto;
        } catch (Exception exception) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(exception.getMessage());
        }
    }

    @PatchMapping
    public ResponseEntity updateClient(@RequestParam Long id, @RequestBody ClientDTO path) {
        try {
            ResponseEntity<ClientDTO> dto = clientService.patch(id, path);
            return dto;
        } catch (Exception exception) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(exception.getMessage());
        }
    }
}
